package jse27;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;


public class MethodInterceptorImpl implements MethodInterceptor {
    final Logger logger = LogManager.getLogger(this.getClass().getName());

    private FactorialService factorialService;

    ArrayList<Integer> parameterArray = new ArrayList<>();
    ArrayList<BigInteger> resultArray = new ArrayList<>();

    public MethodInterceptorImpl(FactorialService factorialService) {
        this.factorialService = factorialService;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        if (method.getName().equals("factorial")){
            Integer index = parameterArray.indexOf(objects[0]);
            if(index!=-1) {
                logger.info("Нашли {}!={} индекс {}",objects[0],resultArray.get(index),index);
                return resultArray.get(index);
            }
            BigInteger rezult = factorialService.factorial((Integer) objects[0]);
            if(10==parameterArray.size()){
                logger.info("Удалим {}!={} индекс 0",parameterArray.get(0),resultArray.get(0));
                parameterArray.remove(0);
                resultArray.remove(0);
            }
            logger.info("Добавим {}!={}",objects[0],rezult);
            parameterArray.add((Integer) objects[0]);
            resultArray.add(rezult);
            return rezult;
        }
        return (methodProxy.invoke(factorialService,objects));
    }
}
