package jse27;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, IOException {

        FactorialService factorialService = new FactorialService();

        MethodInterceptor methodInterceptor = new MethodInterceptorImpl(factorialService);

        FactorialService factorialServiceProxy = (FactorialService) Enhancer.create(FactorialService.class,methodInterceptor);

        for (int i=0;i<11;i++) System.out.println(factorialServiceProxy.factorial(i));
        for (int i=5;i<15;i++) System.out.println(factorialServiceProxy.factorial(i));

    }




}
