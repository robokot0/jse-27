package jse27;

import com.google.common.math.BigIntegerMath;

import java.math.BigInteger;

public class FactorialService {
    public BigInteger factorial(Integer number){
        return BigIntegerMath.factorial(number);
    }
}
